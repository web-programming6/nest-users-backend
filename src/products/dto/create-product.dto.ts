import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';

export class CreateProductDto {
  @MinLength(8)
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
